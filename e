#!/bin/bash
test -e "/home/saadnpq/.spacemacs.d/server/server"
if [ $? -eq 0 ]
then
    emacsclient -s "/home/saadnpq/.spacemacs.d/server/server" -nqca "nvim" "$@"
else
    emacs  --eval '(progn (setq server-socket-dir "/home/saadnpq/.spacemacs.d/server") (server-mode))' "$@"
fi
